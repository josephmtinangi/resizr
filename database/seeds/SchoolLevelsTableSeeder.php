<?php

use App\Models\SchoolLevel;
use Illuminate\Database\Seeder;

class SchoolLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolLevel::truncate();

        SchoolLevel::create([
        	'name' => 'Primary School',
        	'slug' => 'primary-school',
        ]);

        SchoolLevel::create([
        	'name' => 'Secondary School',
        	'slug' => 'secondary-school',
        ]);
    }
}
