<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Region::class, function (Faker $faker) {
    $name = $faker->unique()->city;
    return [
        'name' => $name,
        'slug' => str_slug($name),
    ];
});
