<?php

use Faker\Generator as Faker;

$factory->define(App\Models\District::class, function (Faker $faker) {

    $name = $faker->unique()->streetName;
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'region_id' => function () {
            return factory('App\Models\Region')->create()->id;
        },
    ];
});
