<?php

use Faker\Generator as Faker;

$factory->define(App\Models\School::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'index_number' => $faker->numberBetween(0, 200),
        'region_id' => function () {
            return factory('App\Models\Region')->create()->id;
        },
        'district_id' => function () {
            return factory('App\Models\District')->create()->id;
        },
    ];
});
