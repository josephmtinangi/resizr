<?php

namespace App\Http\Controllers;

class PhotoController extends Controller
{
    public function index()
    {
        $photos = auth()->user()->photos;
        return view('photos.index', compact('photos'));
    }

    public function create()
    {
        return view('photos.create');
    }

    public function upload($id)
    {
        $school = auth()->user()->schools()->findOrFail($id);

        request()->validate([
            'file_id' => 'string|required|url',
        ]);

        $file_id = request()->file_id;

        $photos = [];

        for ($i = 0; $i < $this->getNumberOfFiles($file_id); $i++) {
            $photos[] = $file_id . 'nth/' . $i . '/';
        }

        if ($school->studentsCount() !== $this->getNumberOfFiles($file_id)) {
            return back();
        }

        foreach ($school->students as $index => $student) {
            $student->photo_hash = bin2hex(random_bytes(20));
            $student->photo_url = $photos[$index];
            $student->save();
        }

        return back();
    }

    public function getNumberOfFiles($file_id)
    {
        return (int)substr($file_id, -2, 1);
    }
}
