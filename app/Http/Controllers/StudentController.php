<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{
    public function index($id)
    {
        $school = auth()->user()->schools()->findOrFail($id);

        $students = $school->students;

        return view('students.index', compact('school', 'students'));
    }

    public function create($id)
    {
        $school = auth()->user()->schools()->findOrFail($id);
        return view('students.create', compact('school'));
    }

    public function upload($id)
    {
        $school = auth()->user()->schools()->findOrFail($id);

        // validation

        $rows = Excel::load(request()->file('file'), function ($reader) {

        })->get();

        foreach ($rows as $row) {
            $student = new Student;
            $student->first_name = $row->first_name;
            $student->middle_name = $row->middle_name;
            $student->last_name = $row->last_name;
            $student->gender = $row->gender;
            $student->index_number = $row->index_number;

            $school->students()->save($student);
        }

        return redirect()->route('schools.students.index', $school->id);
    }

    public function previewPhotoEntryForms($id)
    {
        $school = auth()->user()->schools()->findOrFail($id);
        return view('pdf.forms.photo-entry', compact('school'));
    }

    public function downloadPhotoEntryForms($id)
    {
        $school = auth()->user()->schools()->findOrFail($id);

        $view = view('pdf.forms.photo-entry', compact('school'))->render();

        $pdf = PDF::loadHtml($view);

        return $pdf->stream($school->name . '.pdf');
    }
}
