<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'index_number',
        'school_id',
    ];

    public function photo($width = 132, $height = 185)
    {
        return $this->photo_url . '-/resize/' . $width . 'x' . $height . '/';
    }
}
