@extends('layouts.dashboard')

@section('title', 'Schools')

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="content">
                    <a href="{{ route('schools.create') }}" class="btn btn-primary">New School</a>
                </div>
            </div>
        </div>        
    </div>

    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Schools</h4>
            </div>
            <div class="content table-responsive table-full-width">
                <table class="table table-hover table-striped">
                    <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Index</th>
                    <th>Region</th>
                    <th>District</th>
                    <th>Level</th>
                    <th>Date</th>
                    </thead>
                    <tbody>
                    @php $i = 1 @endphp
                    @foreach($schools as $school)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>
                                <a href="{{ route('schools.show', $school) }}">
                                    {{ $school->name }}
                                </a>
                            </td>
                            <td>{{ $school->index_number }}</td>
                            <td>{{ $school->region->name }}</td>
                            <td>{{ $school->district->name }}</td>
                            <td>{{ $school->schoolLevel->name }}</td>
                            <td>{{ $school->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>        
    </div>

@endsection
