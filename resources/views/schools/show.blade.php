@extends('layouts.dashboard')

@section('title', $school->name)

@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">{{ $school->name }}</h4>
            </div>
            <div class="content">
               
               <div class="row">
                   <div class="col-md-6">
                       <div class="table table-responsive">
                           <table class="table table-bordered">
                               <tbody>
                                   <tr>
                                       <th>Name</th>
                                       <td>{{ $school->name }}</td>
                                   </tr>
                                   <tr>
                                       <th>Index</th>
                                       <td>{{ $school->index_number }}</td>
                                   </tr>
                                   <tr>
                                       <th>Region</th>
                                       <td>{{ $school->region->name }}</td>
                                   </tr>
                                   <tr>
                                       <th>District</th>
                                       <td>{{ $school->district->name }}</td>
                                   </tr>
                                   <tr>
                                       <th>Students</th>
                                       <td>{{ $school->studentsCount() }}</td>
                                   </tr>
                                   <tr>
                                       <td>&nbsp;</td>
                                       <td>
                                           <a href="{{ route('schools.students.index', $school->id) }}" class="btn btn-primary">Students</a>
                                       </td>
                                   </tr>
                               </tbody>
                           </table>
                       </div>                       
                   </div>
               </div>

            </div>
        </div>
    </div>

@endsection
