<li class="{{ Request::is('schools') ? 'active' : '' }}">
    <a href="{{ route('schools.index') }}">
        <i class="pe-7s-display1"></i>
        <p>Schools</p>
    </a>
</li>