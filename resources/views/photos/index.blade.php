@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Photos</div>

                <div class="panel-body">
                   @foreach($photos->chunk(6) as $photoSet)
                        <div class="row">
                            @foreach($photoSet as $photo)
                                <div class="col-sm-2 col-xs-2">
                                    <img src="{{ asset('storage/' . $photo->path) }}" class="img-responsive thumbnail" alt="Image">
                                </div>
                            @endforeach
                        </div>
                   @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
