<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $school->name }}</title>
	<link rel="stylesheet" href="{{ asset('dashboard/css/bootstrap.min.css') }}">
	<style>
		body {
			font-family: 'Times New Roman';
		}
		.header {
			text-align: center;
		}
		h2 {
			padding: 0;
			margin: 5px;
			text-decoration: underline;
			font-weight: 600;
		}
		.main {
			margin-top: 50px;
			margin-bottom: 50px;
			height: 600px;
		}
		.student-info {
			font-weight: bold;
			padding-top: 4px;
		}
	</style>
</head>
<body>
	
	<div class="header">
		<h2>THE NATIONAL EXAMINATIONS COUNCIL OF TANZANIA</h2>
		<h2>PHOTO ENTRY FORM PSLE {{ date('Y') }}</h2>
		<h2>{{ $school->index_number }} {{ strtoupper($school->name) }}</h2>	
	</div>

	<div class="main">
		<div class="container">
			@foreach($school->students->chunk(7) as $studentsSet)
				<div class="row">
					@foreach($studentsSet as $student)
						<div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 text-center">
							<img src="{{ $student->photo() }}" alt="">
							<div class="student-info">
								{{ strtoupper($student->index_number) }} <br>
								{{ strtoupper($student->first_name) }} {{ strtoupper($student->middle_name) }} {{ strtoupper($student->last_name) }}<br>
								SEX: {{ $student->gender }} <br>
							</div>
						</div>
					@endforeach
				</div>
			@endforeach			
		</div>
	</div>

	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p class="lead">
						<strong>
							I____________________the head teacher of___________________, I declare that the information above is correct.
						</strong>
					</p>
				</div>			
			</div>
			<div class="row">
				<div class="col-lg-6">
					<p class="lead">
						<strong>
							Signature: ________________________
						</strong>
					</p>		
				</div>
				<div class="col-lg-6">
					<p class="lead">
						<strong>
							Stamp
						</strong>
					</p>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
