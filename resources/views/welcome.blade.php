@extends('layouts.app')

@section('content')

    <div class="jumbotron">
        <div class="container">
            <h1>Resize photos</h1>
            <p>Awesomeness</p>
            <p>
                <a href="{{ route('photos.create') }}" class="btn btn-primary btn-lg">Upload</a>
            </p>
        </div>
    </div>

@endsection

