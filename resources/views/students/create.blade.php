@extends('layouts.dashboard')

@section('title', $school->name)

@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Upload Students</h4>
            </div>
            <div class="content">
                
                <form action="{{ route('schools.students.upload', $school->id) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input type="file" name="file" required>
                    </div>

                    <button type="submit" class="btn btn-primary">Upload</button>
                </form>

            </div>
        </div>
    </div>

@endsection
