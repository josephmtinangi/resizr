@extends('layouts.dashboard')

@section('title', $school->name . ' Students')

@section('content')

    @if($school->students->count())

        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Upload Photos</h4>
                    </div>
                    <div class="content">

                        <form action="{{ route('schools.students.photos.upload', $school->id) }}" method="POST" role="">
                            {{ csrf_field() }}

                            <input type="hidden" role="uploadcare-uploader" name="file_id" data-upload-url-base=""
                                   data-tabs="file" data-multiple=""/>

                            <button type="submit" class="btn btn-primary">Upload</button>
                        </form>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Photo Entry Forms</h4>
                    </div>
                    <div class="content">

                        <div class="row">
                            <div class="col-sm-6">
                                <a href="{{ route('schools.students.photo-entry-forms.preview', $school->id) }}" class="btn btn-primary">Preview</a>
                            </div>
                            <div class="col-sm-6">
                               <form action="{{ route('schools.students.photo-entry-forms.download', $school->id) }}" method="POST">
                                   {{ csrf_field() }}

                                   <button type="submit" class="btn btn-default">Download</button>
                               </form>                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{ $school->name }} Students</h4>
                </div>
                <div class="content {{ $school->students->count() ? 'table-responsive table-full-width' : '' }}">
                    @if($school->students->count())
                        <table class="table table-hover table-bordered">
                            <thead>
                            <th>ID</th>
                            <th>Index</th>
                            <th>First name</th>
                            <th>Middle name</th>
                            <th>Last name</th>
                            <th>Gender</th>
                            <th>Photo</th>
                            </thead>
                            <tbody>
                            @php $i = 1 @endphp
                            @foreach($students as $student)
                                <tr>
                                    <td>{{ $i++ }}.</td>
                                    <td>{{ $student->index_number }}</td>
                                    <td>{{ $student->first_name }}</td>
                                    <td>{{ $student->middle_name }}</td>
                                    <td>{{ $student->last_name }}</td>
                                    <td>{{ $student->gender }}</td>
                                    <td>
                                        @isset($student->photo_url)
                                        <img src="{{ $student->photo() }}" alt="">
                                        @endisset
                                        @empty($student->photo_url)
                                            <i class="pe-7s-user icon-user"></i>
                                        @endempty
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else

                        <div class="description text-center">
                            <p>
                                No Students
                            </p>
                            <a href="{{ route('schools.students.create', $school->id) }}"
                               class="btn btn-primary">Upload</a>
                        </div>

                    @endif

                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>
        UPLOADCARE_LIVE = false;
        UPLOADCARE_IMAGES_ONLY = true;
    </script>

    {!! Uploadcare::scriptTag() !!}
    
@endpush

