<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {

    Route::resource('photos', 'PhotoController');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('schools', 'SchoolController');

Route::get('schools/{school}/students/photo-entry-forms/preview', 'StudentController@previewPhotoEntryForms')->name('schools.students.photo-entry-forms.preview');

Route::post('schools/{school}/students/photo-entry-forms/download', 'StudentController@downloadPhotoEntryForms')->name('schools.students.photo-entry-forms.download');


Route::resource('schools.students', 'StudentController');

Route::post('schools/{school}/students', 'StudentController@upload')->name('schools.students.upload');

Route::post('schools/{school}/students/photos', 'PhotoController@upload')->name('schools.students.photos.upload');

