<?php

return [
    'public_key' => env('UPLOADCARE_PUBLIC_KEY'),
    'private_key' => env('UPLOADCARE_SECRET_KEY'),
];